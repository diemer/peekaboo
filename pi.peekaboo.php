<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
$plugin_info = array(
        'pi_name' => 'Peekaboo Siblings',
        'pi_version'    => '1.0',
				'pi_author' => 'Dan Diemer, Travis Schmeisser, Jack McDade',
				'pi_author_url'	=> 'http://bitbucket.com/diemer',
				'pi_description' => 'Extends the Structure module, allowing pages to see their siblings even the sibling is hidden.',
				'pi_usage' => Peekaboo::usage()
        );
 
require_once(PATH_THIRD.'structure/sql.structure.php');
require_once(PATH_THIRD.'structure/mod.structure.php');
 
class Peekaboo extends Structure {
     
    /**
     * Constructor function
     */
    function __construct() {
         
        // run standard Channel module constructor
        parent::Structure();
				$this->EE =& get_instance();
    }


		/**
		 * Get selective data on all Structure Channels
		 *
		 * @return array
		 */
		private function get_selective_data($site_id, $current_id, $branch_entry_id, $mode, $show_depth, $max_depth, $status, $include, $exclude, $show_overview, $rename_overview, $show_expired, $show_future,$siblings=FALSE)
		{
			$parent_id = $this->sql->get_parent_id($current_id);
			
			$settings = $this->sql->get_settings();
			$trailing_slash = isset($settings['add_trailing_slash']) && $settings['add_trailing_slash'] === 'y';

			$pages = $this->sql->get_site_pages();

			/*
			Trimming Control Params:

			The tree trimmer loads all the entire tree/branch for all nodes
			within the branch_entry_id (which is set by 'start_from' param).

			The trimmer then removes nodes based on the following:
			start_from/branch_entry_id = The root of this nav
			show_depth =	Depth past 'start_from' node which should always be shown
							(use -1 to disable will also disable 'expand_depth')
			expand_depth =	If current node is at the edge, or past the edge of show_depth
							then it will keep (aka expand) this much further
							(use -1 to disable note: only a depth of one currently works)

			---
			Then something crazy happens- purify_bloodlines is called on the current node
			removing all cousins, 2nd cousins etc.

			To prevent un-wanted carnage the active sub-branch (the limb which contains the
			current node on it) can be severed from it's parent ($node->parent = NULL;)
			stopping the bloodline.
			---

			max_depth =		Depth past 'start_from' which you never want to see. Happens
							last which means it will over-ride all the others.
							(use -1 to disable)
			*/

			switch ($mode)
			{
				case 'full':
					// show everything
					$branch_entry_id = 0;	// start from root
					$show_depth = -1;		// defaults to show full tree, passed by the param
					$expand_depth = -1;		// don't trim past current
					// $max_depth = -1;	(can be specified by tag)
					$current_id = FALSE;
					break;
				case 'main':
					// show top nav but never any children
					$branch_entry_id = 0;	// start from root
					$show_depth = -1;		// show full tree
					$expand_depth = -1;		// don't trim past current
					$max_depth = 1;			// only show top level
					$current_id = FALSE;
					break;
				case 'sub':
					$expand_depth = 1;		// show child of current node
					// am I a listing?
					if ($current_id !== FALSE && $this->sql->is_listing_entry($current_id))
						$current_id = $parent_id;
					break;
			}

			if ($show_depth == 'all')
				$show_depth = -1;

			$status = strtolower($status);
			$status_exclude = FALSE;
			if (strncmp($status, 'not ', 4) == 0)
			{
				$status_exclude = TRUE;
				$status = substr($status, 4);
			}
			$statuses = explode('|', $status);

			if ( ! is_array($include))
				$include = array_filter(explode('|', $include), 'ctype_digit');

			if ( ! is_array($exclude))
				$exclude = array_filter(explode('|', $exclude), 'ctype_digit');

			// ---
			// Retreive branch data from DB
			// ---

			// generate flash-data cache name
			$cache_name = 'root='.$branch_entry_id;
			if (count($exclude))
			{
				sort($exclude, SORT_NUMERIC);
				$cache_name .= '-'.implode(',', $exclude);
			}

			// check the flash-data cache
			$results = @$this->EE->session->cache['structure'][$cache_name];
			$results = '';
			if ( ! is_array($results))
			{
				$where_exclude = '';

				foreach ($exclude as $id)
				{
					if ($id != '' && array_key_exists($id, $pages['uris']))
						$where_exclude .= " AND structure.lft NOT BETWEEN (SELECT lft FROM exp_structure WHERE entry_id = '$id') AND (SELECT rgt FROM exp_structure WHERE entry_id = '$id')";
				}

				// $where_include = '';
				// foreach ($include as $id)
				// {
				// 	if ($id != '' && array_key_exists($id, $pages['uris']))
				// 		$where_include .= " AND structure.lft BETWEEN (SELECT lft FROM exp_structure WHERE entry_id = '$id') AND (SELECT rgt FROM exp_structure WHERE entry_id = '$id')";
				// }

				$timestamp = ($this->EE->TMPL->cache_timestamp != '') ? $this->EE->TMPL->cache_timestamp : $this->EE->localize->now;

				if ($show_future == 'no')
				{
					$where_exclude .= " AND (structure.entry_id = 0 OR titles.entry_date < ".$timestamp.") ";
				}

				if ($show_expired == 'no')
				{
					$where_exclude .= " AND (structure.entry_id = 0 OR titles.expiration_date = 0 OR titles.expiration_date > ".$timestamp.") ";
				}


				$sql = "SELECT structure.*, titles.title, titles.entry_date, titles.expiration_date,  LOWER(titles.status) AS status
						FROM
							exp_structure AS structure
							LEFT JOIN exp_channel_titles AS titles
								ON (structure.entry_id = titles.entry_id)
							JOIN (
								SELECT entry_id, lft, rgt
								FROM exp_structure
								WHERE entry_id = '$branch_entry_id' AND site_id IN (0,'$site_id')
							) AS root_node
								ON (structure.lft BETWEEN root_node.lft AND root_node.rgt)
									OR structure.entry_id = root_node.entry_id
						WHERE
							structure.site_id IN (0,'$site_id')
							AND (titles.entry_id IS NOT NULL OR structure.entry_id = 0)
							$where_exclude
						ORDER BY structure.lft";
				// echo $sql;
				$query = $this->EE->db->query($sql);
				$results = $query->result_array();
				$query->free_result();

				// -------------------------------------------
				// 'structure_get_selective_data_results' hook.
				//
					if ($this->EE->extensions->active_hook('structure_get_selective_data_results') === TRUE)
					{
						$results = $this->EE->extensions->call('structure_get_selective_data_results', $results);
					}
				//
				// -------------------------------------------

				$this->EE->session->cache['structure'][$cache_name] = $results;
			}

			// ---
			// Return empty array with no nav
			// ---
			if (count($results) == 0)
				return array();

			// ---
			// Build branch tree and trim
			// ---

			$tree = structure_leaf::build_from_results($results);

			// find the current page in the tree
			$cur_leaf = FALSE;
			if ($current_id !== FALSE)
				$cur_leaf = $tree->find_ancestor('entry_id', $current_id);
			// note: if cur_leaf = FALSE then the current page is not in this sub nav

			if ($cur_leaf === FALSE)
			{
				// the current page is not in this branch
				// use root as current
				$cur_leaf = $tree;
			}

			// limit the shown depth (-1 for show all)
			if ($show_depth >= 0)
			{
				foreach ($tree->children as $child)
				{
					if ($child->has_ancestor($cur_leaf))
					{
						$cur_depth = $cur_leaf->depth();
						if ($cur_depth < $show_depth)
						{
							// not past show_depth yet (no expansion)
							$child->prune_children($show_depth-1);
							// prevent purify_bloodlines from working
							$cur_leaf->parent = NULL;
						}
						elseif ($expand_depth >= 0)
						{
							// expand past current node
							// while preserving show_depth of other branches
							$cur_leaf->prune_children($expand_depth);
							foreach ($child->children as $grandchild)
							{
								if ($grandchild->has_ancestor($cur_leaf))
								{
									// protect non-active branches from
									// purify_bloodlines
									$grandchild->parent = NULL;
								}
								else
								{
									// but don't forget to prune them to
									// the correct show_depth
									$grandchild->prune_children($show_depth-2);
								}
							}
						}
						else
						{
							$child->prune_children($show_depth-1);
							// prevent purify_bloodlines from working
							$cur_leaf->parent = NULL;
						}
					}
					else
					{
						// keep show_depth of non-active branches
						$child->prune_children($show_depth-1);
					}
				}
			}

			// gets rid of cousins and 2nd cousins
			// keeps children, parents and uncles
			$cur_leaf->purify_bloodline();

			// limit overall depth shown (-1 for infinite)
			if ($max_depth >= 0)
				$tree->prune_children($max_depth);

			// limit based on status
			if (count($statuses))
			{
				if ($tree->row['entry_id'] != 0) // don't test structure ROOT (would always fail)
				{
					if ($tree->is_of_value('status', $statuses, $status_exclude))
						return array(); // root node removed based on critera
				}

				$tree->selective_prune('status', $statuses, $status_exclude);
			}

			if ($this->EE->TMPL->fetch_param('override_hidden_state', FALSE) != 'yes')
				if(!$siblings)
					$tree->selective_prune('hidden', array('y'), TRUE);


			// limit to 'include' ids
			if (count($include))
			{
				array_unshift($include, $branch_entry_id); // add current root node as valid
				if ($tree->is_of_value('entry_id', $include, FALSE))
					return array(); // root node removed based on critera

				$tree->selective_prune_alt('entry_id', $include, FALSE);
			}

			// add 'depth' to the rows
			// (happens here, because the tree is already trimmed = less waste)
			$tree->add_row_depth();

			// rebuild results from what is left in the tree
			$results = $tree->get_results();

			if ($show_overview == 'yes')
			{
				// add sql to get this entry
				$overview = $this->sql->get_overview($branch_entry_id);
				$rename_overview = $rename_overview == 'title' ? $overview['title'] : $rename_overview; // override if "title"

				$overview['title'] = $rename_overview;
				$overview['overview'] = $rename_overview;

				array_unshift($results, $overview);
			}

			$data = array();
			foreach ($results as $row)
			{
				if ( ! isset($row['entry_id'])) continue;

				$data[$row['entry_id']] = $row;
				$data[$row['entry_id']]['uri'] = $this->EE->functions->create_page_url($pages['url'], $pages['uris'][$row['entry_id']], $trailing_slash);
				$data[$row['entry_id']]['slug'] = $pages['uris'][$row['entry_id']];
				$data[$row['entry_id']]['classes'] = array();
				$data[$row['entry_id']]['ids'] = array();
				// echo $data[$row['entry_id']]['uri'].' ';
			}

			return $data;
		}


		/** -------------------------------------
		/**	 Tag: siblings
		/** -------------------------------------*/
		function siblings()
		{
			$tagdata = $this->EE->TMPL->tagdata;
			
			// Fetch core data
			$uri = $this->sql->get_uri();
			$site_pages = $this->sql->get_site_pages();


			// Get entry_id. Parameter override available.
			$entry_id = $this->EE->TMPL->fetch_param('entry_id', array_search($uri, $site_pages['uris']));
			$parent_id = $this->sql->get_parent_id($entry_id);
			
			if ($parent_id == $this->sql->get_home_page_id())
				$parent_id = 0;
			
			$site_id = $this->EE->config->item('site_id');
			$status = $this->EE->TMPL->fetch_param('status', 'open');
			$include = $this->EE->TMPL->fetch_param('include', array());
			$exclude = $this->EE->TMPL->fetch_param('exclude', array());
			$show_expired = $this->EE->TMPL->fetch_param('show_expired', 'no');
			$show_future = $this->EE->TMPL->fetch_param('show_future_entries', 'no');
			
			$custom_title_fields = $this->sql->create_custom_titles(TRUE);
			
			$pages = $this->get_selective_data($site_id, $entry_id, $parent_id, 'sub', 1, -1, $status, $include, $exclude, FALSE, FALSE, $show_expired, $show_future,TRUE); // Get parent and all children
			
			if ( ! is_array($pages) || count($pages) == 0)
				return NULL;
			
			$next = array();
			$prev = array();
			// echo $entry_id;
			// Filter out pages not on same level
			foreach($pages as $key => $page)
			{
				if ($entry_id && array_key_exists($entry_id, $pages) && $page['depth'] != $pages[$entry_id]['depth'])
					unset($pages[$key]);
			}
			
			$pages = array_values($pages); // Zero index for easy array navigation
			
			foreach($pages as $key => $page)
			{
				if ($page['entry_id'] == $entry_id)
				{
					if (array_key_exists($key-1, $pages) && $page['depth'] == $pages[$key-1]['depth'])
					{
						$prev[] = array(
							'title' => $custom_title_fields !== FALSE ? $custom_title_fields[$pages[$key-1]['entry_id']] : $pages[$key-1]['title'],
							'url' => $pages[$key-1]['uri'],
							'entry_id' => $pages[$key-1]['entry_id'],
							'parent_id' => $pages[$key-1]['parent_id'],
							'channel_id' => $pages[$key-1]['channel_id'],
							'status' => $pages[$key-1]['status']
						);
					}
					if (array_key_exists($key+1, $pages) && $page['depth'] == $pages[$key+1]['depth'])
					{
						$next[] = array(
							'title' => $custom_title_fields !== FALSE ? $custom_title_fields[$pages[$key+1]['entry_id']] : $pages[$key+1]['title'],
							'url' => $pages[$key+1]['uri'],
							'entry_id' => $pages[$key+1]['entry_id'],
							'parent_id' => $pages[$key+1]['parent_id'],
							'channel_id' => $pages[$key+1]['channel_id'],
							'status' => $pages[$key+1]['status']
						);
					}
					
				}
			}
			$variable_row = array('prev' => $prev, 'next' => $next);
			$vars[] = $variable_row;
			
			return $this->EE->TMPL->parse_variables($tagdata, $vars);
		}

    // --------------------------------------------------------------------

    /**
     * Usage
     *
     * This function describes how the plugin is used.
     *
     * @access  public
     * @return  string
     */
    public static function usage()
    {
        ob_start();  ?>

Uses the syntax {exp:peekaboo:siblings} and works identically to the builtin Structure siblings tag

    <?php
        $buffer = ob_get_contents();
        ob_end_clean();

        return $buffer;
    }
    // END
}
