Peekaboo for Structure
======================

Use this plugin to let pages see their siblings even when they are hidden

Usage
-----

The plugin functions identically to the [{exp:structure:siblings}](http://buildwithstructure.com/tags#tag_siblings) tag, but uses the syntax {exp:peekaboo:siblings} instead.
